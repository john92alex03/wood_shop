CREATE TABLE users (
	id_user SERIAL NOT NULL,
	first_name VARCHAR(30) NOT NULL,
	last_name VARCHAR(30) NOT NULL,
	email_user VARCHAR(60) NOT NULL,
	password_current VARCHAR(30) NOT NULL,
	password_last VARCHAR(30) NOT NULL,
	phone_user BIGINT NOT NULL,
	addres_user VARCHAR(50) NOT NULL,
	departament_user VARCHAR(40) NOT NULL,
	city_addres_user VARCHAR(40) NOT NULL,
	creation_current TIMESTAMP,
	modify_user TIMESTAMP,
	PRIMARY KEY(id_user)
);

CREATE TABLE categories (
	id_category SERIAL NOT NULL,
	name_category VARCHAR(40) NOT NULL,
	PRIMARY KEY(id_category)
);

CREATE TABLE products (
	id_product SERIAL NOT NULL UNIQUE,
	name_product VARCHAR(100) NOT NULL,
	description_product VARCHAR(500) NOT NULL,
	stock_product BOOLEAN DEFAULT TRUE,
	price_product INTEGER DEFAULT 0,
	heigth_product INTEGER DEFAULT 0,
	width_product INTEGER DEFAULT 0,
	depth_product INTEGER DEFAULT 0,
	material_product VARCHAR(50),
	door_product INTEGER DEFAULT(0),
	drawer_product INTEGER DEFAULT(0),
	creation_product TIMESTAMP,
	modify_product TIMESTAMP,
	category_product INT REFERENCES categories(id_category) NOT NULL,
	PRIMARY KEY(id_product)
);



INSERT INTO users (
	first_name,
	last_name,
	email_user,
	password_current,
	password_last,
	phone_user,
	addres_user,
	departament_user,
	city_addres_user,
	creation_current,
	modify_user
) VALUES(
	'CARLOS',
	'ESCOBAR',
	'CORREO@GMAIL.COM',
	'Miclave12',
	'Miclave12',
	3024589545,
	'CR 10 # 26 - 05',
	'VALLE DEL CAUCA',
	'CALI',
	CURRENT_TIMESTAMP,
	CURRENT_TIMESTAMP
)


INSERT INTO categories (
	name_category
) VALUES (
	'SILLA'
),
(
	'MUEBLE'
),
(
	'ARMARIO'
),
(
	'COMEDOR'
),
(
	'CAMA'
),
(
	'ESPEJOS'
)


INSERT INTO products (
	name_product,
	description_product,
	stock_product,
	price_product,
	heigth_product,
	width_product,
	depth_product,
	material_product,
	creation_product,
	modify_product,
	category_product
) VALUES (
	'silla italiana',
	'esto es una silla en madera en cedro con varios acabados y buen diseño',
	false,
	120000,
	150,
	80,
	50,
	'madera cedro',
	CURRENT_TIMESTAMP,
	CURRENT_TIMESTAMP,
	4
)



INSERT INTO products (
	name_product,
	description_product,
	price_product,
	heigth_product,
	width_product,
	depth_product,
	material_product,
	creation_product,
	modify_product,
	category_product
) VALUES (
	'armario',
	'Armario de alta calidad en madera de cedro, una opción elegante y duradera para organizar y embellecer tu espacio',
	1200000,
	170,
	120,
	80,
	'madera cedro',
	CURRENT_TIMESTAMP,
	CURRENT_TIMESTAMP,
	2
)



INSERT INTO products (
	name_product,
	description_product,
	price_product,
	heigth_product,
	width_product,
	material_product,
	creation_product,
	modify_product,
	category_product
) VALUES (
	'cama en cedro',
	'Cama de ensueño en exquisita madera de cedro, fusionando elegancia y comodidad para brindarte noches de descanso inigualables.',
	900000,
	80,
	150,
	'madera cedro',
	CURRENT_TIMESTAMP,
	CURRENT_TIMESTAMP,
	1
)



INSERT INTO products (
	name_product,
	description_product,
	stock_product,
	price_product,
	heigth_product,
	width_product,
	material_product,
	creation_product,
	modify_product,
	category_product
) VALUES (
	'comedor en 4 puestos',
	'Transforma tu espacio de comedor con elegancia atemporal. Este juego de comedor de 4 puestos en madera de cedro ofrece una experiencia gastronómica única',
	false,
	1400000,
	60,
	70,
	'madera cedro',
	CURRENT_TIMESTAMP,
	CURRENT_TIMESTAMP,
	3
)


INSERT INTO products (
	name_product,
	description_product,
	price_product,
	heigth_product,
	width_product,
	material_product,
	creation_product,
	modify_product,
	category_product
) VALUES (
	'comedor en 6 puestos',
	'Transforma tu espacio de comedor con elegancia atemporal. Este juego de comedor de 6 puestos en madera de cedro ofrece una experiencia gastronómica única',
	1800000,
	70,
	80,
	'madera cedro',
	CURRENT_TIMESTAMP,
	CURRENT_TIMESTAMP,
	3
)


CREATE TABLE imageProducts (
	id_image SERIAL NOT NULL,
	name_image VARCHAR(60) NOT NULL,
	priority_image INT NOT NULL,
	creation_image TIMESTAMP,
	id_product SERIAL REFERENCES products(id_product) NOT NULL,
	PRIMARY KEY(id_image)
);

INSERT INTO imageProducts ( name_image, priority_image,creation_image, id_product ) VALUES ( 'cama_3_th5fzp', 1 , CURRENT_TIMESTAMP , 3);

INSERT INTO imageProducts ( name_image, priority_image,creation_image, id_product ) VALUES ( 'cama_1_si03ou', 2 , CURRENT_TIMESTAMP , 3);

INSERT INTO imageProducts ( name_image, priority_image,creation_image, id_product ) VALUES ( 'cama_2_svaxwy', 2 , CURRENT_TIMESTAMP , 3);

INSERT INTO imageProducts ( name_image, priority_image,creation_image, id_product ) VALUES ( 'cama_6_k5zzks', 2 , CURRENT_TIMESTAMP , 3);

INSERT INTO imageProducts ( name_image, priority_image,creation_image, id_product ) VALUES ( 'cama_5_lqd47k', 2 , CURRENT_TIMESTAMP , 3);

INSERT INTO imageProducts ( name_image, priority_image,creation_image, id_product ) VALUES ( 'cama_4_pfl5pz', 2 , CURRENT_TIMESTAMP , 3);

INSERT INTO imageProducts ( name_image, priority_image,creation_image, id_product ) VALUES ( 'cama_7_yfdtra', 2 , CURRENT_TIMESTAMP , 3);


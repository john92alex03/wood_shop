"use client";
import { useState } from 'react'
import Link from 'next/link'

export default function Navigate() {

  const [ activeNavigation, setActiveNavigation ] = useState({ home: true, services: false, about: false, contact: false })

  return (
    <nav className="border-gray-200 bg-gray-50 dark:bg-gray-200 bg-opacity-5 dark:border-gray-700 h-24 flex items-center">
      <div className="flex items-center w-full p-4">
        <h1 className='flex justify-center text-2xl font-light pl-20'>food <span className='font-semibold text-3xl ml-3'> Shop </span></h1>
        <div className=" w-full  md:block md:w-auto ml-10" id="navbar-solid-bg">
          <ul className="capitalize flex font-semibold text-lg justify-items-center mt-4 rounded-lg bg-gray-50 md:flex-row md:space-x-4 md:mt-0 md:border-0 md:bg-transparent dark:bg-gray-800 md:dark:bg-transparent dark:border-gray-700 w-full ">
            <li>
              <Link
                href="/"
                className={`block w-20 py-2 hover:text-blue-500 bg-blue-700 rounded md:bg-transparent md:p-0 md:dark:bg-transparent  ${activeNavigation.home ? 'text-blue-500' : 'text-gray-900'}`} 
                aria-current="page"
                onClick={()=> setActiveNavigation({ home: true, services: false, about: false, contact: false })}
              >
                Inicio
              </Link>
            </li>
            <li>
              <Link
                href="/services"
                className={`block w-20  pl-3 pr-4rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 hover:text-blue-500 md:p-0 md:dark:hover:bg-transparent ${activeNavigation.services ? 'text-blue-500' : 'text-gray-900'}`}
                onClick={()=> setActiveNavigation({ home: false, services: true, about: false, contact: false })}
              >
                Servicios
              </Link>
            </li>
            <li>
              <Link
                href="/shop/about"
                className={`block w-40  pl-3 pr-4rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 hover:text-blue-500 md:p-0 md:dark:hover:bg-transparent ${activeNavigation.about ? 'text-blue-500' : 'text-gray-900'}`}
                onClick={()=> setActiveNavigation({ home: false, services: false, about: true, contact: false })}
              >
                ¿quienes somos?
              </Link>
            </li>
            <li>
              <Link
                href="/contact"
                className={`block w-20  pl-3 pr-4rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 hover:text-blue-500 md:p-0 md:dark:hover:bg-transparent ${activeNavigation.contact ? 'text-blue-500' : 'text-gray-900'}`}
                onClick={()=> setActiveNavigation({ home: false, services: false, about: false, contact: true })}
              >
                Contacto
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

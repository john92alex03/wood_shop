
import { useState, useEffect } from 'react'
import { useRouter } from 'next/navigation'
import { useDispatch } from 'react-redux'
import { updateCategorie } from '../store/product/productSlice'
import  Link from 'next/link'


export default function See_items  (props: any) {

  const router = useRouter()

  const dispatch = useDispatch()

  const [ countList, setCountList ] = useState<number>(0)

  const { dataItems } = props

 return (
  <section className="h-auto my-16  w-auto flex-col justify-center items-center flex">    
      
        <ul className='grid grid-cols-3'>
          { dataItems.map((item: any)=> 
            (<li key={item.idItem} id={item.idItem} className='w-auto m-4 bg-white h-auto border border-gray-200 rounded-lg shadow'  >
              <Link href={`/shop/items_shop`} onClick={()=> {
                                            dispatch(updateCategorie(item.idCategory))
                                         } } >
                <img 
                  className="rounded-t-lg w-full h-64" 
                  src={`./${item.photo}`} 
                  alt="image-items-comedor"  
                />
              </Link>
              <aside className="p-5">
                <h4 className="mb-2 text-2xl">
                  {item.title}
                </h4>
                <p>
                  {item.description}
                </p>
                <button  
                  className="font-medium px-5 py-2.5 text-base mr-2 mb-2 mt-3 bg-amber-400 rounded-lg focus:ring-2 focus:ring-amber-200 font-sans" 
                  type="button"  
                  onClick={ ()=> {
                        dispatch(updateCategorie(item.idCategory))
                        router.push(`/shop/items_shop`)
                    } 
                  }
                >
                  ver mas
                </button>
              </aside>
            </li>)
          ) }
        </ul>     
      
  </section>
 )
}
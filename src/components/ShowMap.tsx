
import Image from 'next/image'

export default function ShowMap () {
  return (
    <div className="h-auto relative bottom-28">
      <div className='relative h-auto w-full text-center flex items-center flex-col'>
        <Image alt='googleMaps' src='/google_maps.jpg' width={1000} height={1000} className='w-8/12 rounded-sm z-20' />
      </div>
    </div>
  )
}
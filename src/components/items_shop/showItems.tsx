
import Link from 'next/link'
import Image from 'next/image'
import { useSelector } from 'react-redux'
import { useEffect, useState } from 'react'

export default function ShowItems (props: any):JSX.Element {
  
  const { setCurrentItems , setPriceItems , items , setSeeFilter, seeFilter } = props

  const useData: any = useSelector((data)=> data)

  const filterItems  = ( itemsList: any ) => {

    let dataItems : any[] = []
    let priceItems : number[] = []

    //  uso de funcion filter para filtrar los elementos por medio de categoria

    console.log(useData.categorie.categorie)

    if(useData.categorie.categorie == 0) {
      itemsList.map((item : any)=> {
                dataItems.push(item)
                priceItems.push(item.price_product)
              }
            )
    
      setSeeFilter(dataItems)
      setCurrentItems(dataItems.length) 
    } else {
      
      let getFilterItems = itemsList.filter((item: any)=> 
        useData.categorie.categorie == item.category_product
      )

      getFilterItems.map((itemFilter: any)=> {
        dataItems.push(itemFilter)
      })

      console.log(getFilterItems)
      setSeeFilter(getFilterItems)
      setCurrentItems(getFilterItems.length)
      priceItems.push(getFilterItems.priceItem)
    }

    setPriceItems(priceItems)
    console.log(seeFilter)
  }
  console.log(seeFilter)
  
  useEffect(()=> {
    filterItems(items)
  },[useData])
  

  return (
    <div className="container flex flex-wrap -m-4 ">

          {
            seeFilter.map((item: any)=> {
                
                return (
                  <div className="lg:w-1/4 md:w-1/2 p-4 w-full shadow-lg" key={item?.id_product}  >
                    <Link href="/shop/items_shop/12" className="block relative h-48 roudend overflow-hidden">
                      <Image alt="image_1" src='/imageItems/image_comedor.jpg' className="object-cover object-center block w-full h-full" width={800} height={800} />
                    </Link>

                    <div className="mt-4">
                      <h3 className="text-gray-500 text-sm tracking-widest title-font mb-1">Categoria: <span className="text-gray-700 font-medium">{item?.category_product}</span></h3>
                      <h2 className="text-gray-900 text-lg title-font font-medium">{item?.name_product}</h2>
                      <p className="mt-1">${item?.price_product}</p>
                    </div>
                  </div>    
                )
              }
            )
                
          }

    </div>
  )
}


const dataItems = [
    {
      categorieItem : '',
      nameItem : '',
      priceItem :0,
      inStock : false,

    }
    // {
    //   categorieItem : '1',
    //   nameItem : 'Silla en madera',
    //   priceItem : 95000,
    //   inStock : false,
    // },
    // {
    //     categorieItem : '4',
    //   nameItem : 'comedor cuatro puestos',
    //   priceItem : 270000,
    //   inStock : true,
      
    // },
    // {
    //   categorieItem : '4',
    //   nameItem : 'comedor seis puestos',
    //   priceItem : 360000,
    //   inStock : true,
    // },
    // {
    //   categorieItem : '3',
    //   nameItem : 'armario 1 puerta',
    //   priceItem : 240000,
    //   inStock : true,
    // },
    // {
    //   categorieItem : '3',
    //   nameItem : 'armario 2 puerta',
    //   priceItem : 570000,
    //   inStock : false,
    // },
    // {
    //   categorieItem : '3',
    //   nameItem : 'armario 3 puerta',
    //   priceItem : 690000,
    //   inStock : false,
    // },
    // {
    //   categorieItem : '1',
    //   nameItem : 'escritorio pequeño',
    //   priceItem : 210000,
    //   inStock : true,
    // },
    // {
    //   categorieItem : '2',
    //   nameItem : 'escritorio mediano',
    //   priceItem : 340000,
    //   inStock : true,
    // },
    // {
    //   categorieItem : '2',
    //   nameItem : 'escritorio grande',
    //   priceItem : 390000,
    //   inStock : false,
    // }
  
]
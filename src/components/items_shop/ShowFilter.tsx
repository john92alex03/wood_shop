
import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { updateCategorie } from '../../store/product/productSlice'

import PriceFilter from '../pure/items_shop/PriceFilter';



export default function ShowFilter (props: any) : JSX.Element {

  const { optionsCategorieItems, currentItems, priceItems , seeFilter, setSeeFilter } = props;

  const dispatch = useDispatch()

  const [ activeCheckbox, setActiveCheckbox ] = useState(0)

  const [ lowPriceFilter, setLowPriceFilter  ] = useState<number>(0)

  const [ highPriceFilter, setHighPriceFilter ] = useState<number>(0)

  const dateCategorie = useSelector((response: any)=> response.categorie.categorie )
  
  return (
    <section className="block w-2/12 space-y-9">
      <div className="">
        <label htmlFor="sortBy" className="block text-base font-medium text-gray-700">
          ordenar por:
        </label>
        <select name="sortBy" id="sortBy" className="mt-1 roundend border-gray-300 bg-gray-100 text-base p-1">
          <option>Ordena por</option>
          <option value='title, DESC'> titulo DESC</option>
          <option value='title, ASC'>titulo ASC</option>
          <option value='price, DESC'>precio DESC </option>
          <option value='price, ASC'>precio ASC </option>
        </select>
      </div>

      <div className="">
        <p className="block text-base text-gray-700 font-medium"> filtros </p>

        <div className="mt-1 space-y-2 w-10/12">

          <details className="overflow-hidden roundend border border-gray-300 [&_summary::-webkit-details-marker]:hidden">
            <summary className="flex cursor-pointer items-center justify-between gap-2 p-3 text-gray-900 transition">
              <span className="text-sm font-medium capitalize"> 
                disponibilidad
              </span>
              <span className="transition group-open:-rotate-180">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke-width="1.5"
                    stroke="currentColor"
                    className="h-4 w-4"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      d="M19.5 8.25l-7.5 7.5-7.5-7.5"
                    />
                  </svg>
                </span>
            </summary>

            <div className="border-t border-gray-200">
              <header className="flex items-center justify-between p-4">
                <span className="text-sm text-gray-700"> 0 seleccionados </span>
                <button type="button" className="text-sm text-gray-900 underline underline-offset-4">
                  reset
                </button>
              </header>

              <ul className="space-y-1 border-t border-gray-200 p-4">
                <li> 
                  <label htmlFor="filterInStock" className="inline-flex items-center gap-2">
                    <input type="checkbox" id="filterInStock" className="h-4 w-4 rounded border-gray-300" />
                    <span className="text-sm font-medium text-gray-700">
                      en stock
                    </span>
                  </label>
                </li>
              </ul>

            </div>

          </details>

          {/* segundo filtro */}

          <PriceFilter priceItems={priceItems} setLowPriceFilter={setLowPriceFilter} setHighPriceFilter={setHighPriceFilter} seeFilter={seeFilter} setSeeFilter={setSeeFilter} />

         { /* detalles filtros categorias  */}

         <details className="overflow-hidden roundend border border-gray-300 [&_summary::-webkit-details-marker]:hidden">
            <summary className="flex cursor-pointer items-center justify-between gap-2 p-3 text-gray-900 transition">
              <span className="text-sm font-medium capitalize"> 
                categorias
              </span>

              <span className="transition group-open:-rotate-180">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke-width="1.5"
                    stroke="currentColor"
                    className="h-4 w-4"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      d="M19.5 8.25l-7.5 7.5-7.5-7.5"
                    />
                  </svg>
                </span>
            </summary>

            <div className="border-t border-gray-200">
              <header className="flex items-center justify-between p-4">
                <span className="text-sm text-gray-700"> {currentItems} seleccionados</span> 
                <button type="button" className="text-sm text-gray-900 underline underline-offset-4" onClick={()=> dispatch(updateCategorie(0)) } >
                  reset
                </button>
              </header>

              <ul className="space-y-1 border-t border-gray-200 p-3">
                
                {
                optionsCategorieItems?.map((categorie:any)=> {
                 
                    return (
                      <li className="flex items-center capitalize h-3/12" key={categorie.id_category} id={categorie.id_category} >
                        <input 
                          type="checkbox"
                          id={categorie.id_category}
                          className="h-4 w-4 roundend border-gray-300 mr-2 " 
                          value={0}
                          onChange={(data)=> {
                            
                            if(data.target.checked){
                              
                              setActiveCheckbox(data.target.checked ? parseInt(data.target.id) : 0)
                              dispatch(updateCategorie(categorie.id_category))
                            } else {
                              setActiveCheckbox(data.target.checked ? parseInt(data.target.id) : 0)
                              dispatch(updateCategorie(0))
                            }
                          }}
                          checked= {activeCheckbox > 0 ? activeCheckbox ==  categorie.id_category : false}
                        />
                        <label htmlFor="filterDiningRoom" className="capitalize h-full" >
                          <span className="text-sm font-medium text-gray-700 capitalize">
                            {categorie.name_category}
                          </span>
                        </label>
                      </li>
                    )
                })
                }
              </ul>

            </div>
            

          </details>

        </div>

      </div>
      
    </section>
  )
}
'use client';

import Image from 'next/image';

import { RiWhatsappFill } from 'react-icons/ri'
import { IoLogoFacebook } from 'react-icons/io'

export default function Footer () {
  return (
    <footer className="flex h-32 bg-stone-300">
      <h1 className='flex items-center text-3xl font-light pl-20'>food <span className='font-semibold text-4xl ml-3'> Shop </span></h1>
      <div className="flex justify-center items-center w-7/12" >
        <ul className="text-lg font-mono font-medium pt-4">
          <li className="hover:cursor-pointer">
            shop
          </li>
          <li className="hover:cursor-pointer">
            contact
          </li>
          <li className="hover:cursor-pointer">
            home
          </li>
        </ul>
      </div>

      <div className='text-4xl w-44 flex items-center justify-between ml-20'>
        <RiWhatsappFill className='text-green-600 hover:cursor-pointer' />
        <IoLogoFacebook className='text-blue-600 hover:cursor-pointer' />
      </div>

      <div className='flex items-center justify-end w-44 ml-14'>
        <span className='font-mono text-xs text-gray-500'>
          © 2023 Copyright:
        </span>
        <Image src='/imagen logo.png' alt='imagenLogo' width={500} height={500} className='h-20 w-20' />
      </div>
    </footer>
  )
}
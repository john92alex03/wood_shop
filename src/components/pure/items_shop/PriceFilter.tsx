

import { useState } from 'react'


export default function PriceFilter (props: any) : JSX.Element {

  const { priceItems, setLowPriceFilter , setHighPriceFilter, seeFilter, setSeeFilter } = props

  const [ lowPrice, setLowPrice ] = useState<number>(0)

  const [ highPrice, setHighPrice ] = useState<number>(0)

  const [ errorPriceFilter, setErrorPriceFilter ] = useState<string>('')

 
  console.log(seeFilter)

  function itemsFilterPrice (items: any[] , setItems?: any  ) {

    console.log(items)
    let itemsFiltered = []
    
    if( lowPrice < highPrice ) {
      itemsFiltered = items.filter((item)=> {
        if ((item.price_product >= lowPrice) && (item.price_product <= highPrice)) {
          return true
        }
      })
    } else if (lowPrice > highPrice) {
      itemsFiltered = items.filter((item)=> {
        if (item.price_product <= highPrice) {
          return true
        }
      })
    } else if (lowPrice == highPrice) {
      setErrorPriceFilter('no deben ser iguales')
      itemsFiltered = items
    }
    setItems(itemsFiltered)
  }
  
  return (

    <details className="overflow-hidden roundend border border-gray-300 [&_summary::-webkit-details-marker]:hidden">
            <summary className="flex cursor-pointer items-center justify-between gap-2 p-3 text-gray-900 transition">
              <span className="text-sm font-medium capitalize"> 
                precio
              </span>

              <span className="transition group-open:-rotate-180">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke-width="1.5"
                    stroke="currentColor"
                    className="h-4 w-4"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      d="M19.5 8.25l-7.5 7.5-7.5-7.5"
                    />
                  </svg>
                </span>
            </summary>

            <div className="border-t border-gray-200">
              <header className="flex items-center justify-between p-4">
                <span className="text-sm text-gray-700"> el precio mas alto es ${Math.max(...priceItems)} </span>
                <button type="button" className="text-sm text-gray-900 underline underline-offset-4">
                  reset
                </button>
              </header>

              <div className=" flex flex-col border-t border-gray-200 p-4">
                <div className="flex justify-between gap-4 flex-col">
                  <label htmlFor="filterPriceFrom" className="flex items-center gap-2">
                    <span className="text-sm text-graY-600">$</span>
                    <input 
                      type="number"
                      id="filterPriceFrom"
                      name="filterPriceFrom"
                      onChange={(res)=> {setLowPrice(parseInt(res.target.value))} }
                      placeholder="Desde"
                      className="w-10/12 roundend-md border-gray-200 bg-gray-100 shadow-sm"
                    />
                  </label>

                  <label htmlFor="filerPriceTo" className="flex items-center gap-2">
                    <span className="text-sm text-gray-600">$</span>
                    <input 
                      type="number"
                      id="filterPriceTo"
                      name="filerPriceTo"
                      onChange={(res)=> setHighPrice(parseInt(res.target.value))}
                      placeholder="Hasta"
                      className="w-10/12 roundend-md border-gray-200 bg-gray-100 shadow-sm" 
                    />
                  </label>
                </div>
                {errorPriceFilter}
                <button 
                  className='flex justify-center mt-4 border border-solid border-blue-400 text-center rounded-md p-0.5 w-auto'
                  onClick={()=> {
                    setLowPriceFilter(lowPrice)
                    setHighPriceFilter(highPrice)

                    itemsFilterPrice(seeFilter, setSeeFilter)
                  }}
                >
                    aplicar
                </button>
              </div>
            </div>

          </details>
  )
}
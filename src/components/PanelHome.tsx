import Link from 'next/link'

export default function PanelHome() {
  return(
    <section className="max-h-96 h-auto my-10 flex items-center flex-col justify-center">    
    <h1 className="mb-10 text-4xl font-semibold font-mono ">Estilo y Durabilidad <br/> <span className="text-center flex justify-center"> para tu Espacio </span> </h1>
    <p className="text-2xl">
      Aquí encontrarás el lugar perfecto para llenar tu hogar con la <br/> <span className="text-center flex justify-center"> esencia natural de la madera </span> 
    </p>

    <article className="pt-10">
      <Link href='/items_shop'>
        <button className="w-32 h-11 bg-amber-400 right-8 rounded-md shadow animate-bounce  text-lg font-medium font-mono">
          empezemos!
        </button>
      </Link>
    </article>
  </section>
  )
}
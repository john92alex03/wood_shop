
import Image from 'next/image'

import { SiWhatsapp } from 'react-icons/si'

export default function BannerPersonalization () :JSX.Element {
  
  return (
    <div className="relative text-center w-full h-auto bottom-20">
      <SiWhatsapp className='text-center w-full text-5xl hover:cursor-pointer text-green-600'  />
      <div className='mt-6 flex flex-col items-center'>
        <h1 className='text-5xl mt-6 font-serif border-b-2 shadow-sm border-gray-300'>Transforma tus sueños en madera</h1>
        <h3 className='text-4xl font-serif mt-4'>Creando espacios unicos y personalizados para tu hogar</h3>
        <p className='w-1/2 text-2xl mt-9 font-serif'>
          descubre la magia de la madera con nuestros servicios de diseño y fabricación a medida. En wood shop, convertimos tus ideas en realidad. Desde muebles hechos a mano hasta revestimientos de madera exquisitos, creamos espacios que reflejan tu estilo y personalidad.
        </p>

        <div className='w-11/12 justify-center h-auto py-6 grid grid-cols-4 items-center text-center'>
          <div className='w-full flex justify-center items-center relative'>
            <div className='h-96 w-96 shadow-lg hover:shadow-amber-500/30 relative text-center flex justify-center'>
              <Image src='/services/diseñoPersonalizado.jpg' alt='diseñoPersonalizado' width={500} height={500} className='h-full w-full hover:blur-sm z-30 hover:z-10 hover:brightness-50'  />
              <h1 className='absolute block text-gray-200 font-mono font-bold text-xl bottom-32 h-auto z-10'>Diseño personalizado</h1>
              <p className='absolute block text-gray-200 font-mono font-semibold bottom-10 text-base w-4/5 mt-2 z-10'>Cada proyecto es único, diseñado según tus gustos y necesidades.</p>
            </div>
          </div>
          <div className='w-full text-center flex justify-center items-center'>
            <div className='h-96 w-96 shadow-lg hover:shadow-amber-500/30 relative text-center flex justify-center'>
              <Image src='/services/artesania_calidad.jpg' alt='diseñoPersonalizado' width={500} height={500} className='h-full w-full hover:blur-sm z-30 hover:z-10 hover:brightness-50'  />
              <h1 className='absolute block text-gray-200 font-mono font-bold text-xl bottom-32 h-auto z-10'>Artesanía de Calidad</h1>
              <p className='absolute block text-gray-200 font-mono font-semibold bottom-10 text-base w-4/5 mt-2 z-10'>Utilizamos las mejores maderas y técnicas artesanales para garantizar durabilidad y belleza.</p>
            </div>
          </div>
          <div className='w-full text-center flex justify-center items-center'>
            <div className='h-96 w-96 shadow-lg hover:shadow-amber-500/30 relative text-center flex justify-center'>
              <Image src='/services/trasnformacion_espacios.jpg' alt='diseñoPersonalizado' width={500} height={500} className='h-full w-full hover:blur-sm z-30 hover:z-10 hover:brightness-50'  />
              <h1 className='absolute block text-gray-200 font-mono font-bold text-xl bottom-32 h-auto z-10'>Transformación de Espacios</h1>
              <p className='absolute block text-gray-200 font-mono font-semibold bottom-10 text-base w-4/5 mt-2 z-10'>Dale vida a tu hogar con nuestros muebles y acabados de madera hechos a mano.</p>
            </div>
          </div>
          <div className='w-full text-center flex justify-center items-center'>
            <div className='h-96 w-96 shadow-lg hover:shadow-amber-500/30 relative text-center flex justify-center'>
              <Image src='/services/profesionales_apasionados.jpg' alt='diseñoPersonalizado' width={500} height={500} className='h-full w-full hover:blur-sm z-30 hover:z-10 hover:brightness-50'  />
              <h1 className='absolute block text-gray-200 font-mono font-bold text-xl bottom-32 h-auto z-10'>Profesionales Apasionados</h1>
              <p className='absolute block text-gray-200 font-mono font-semibold bottom-10 text-base w-4/5 mt-2 z-10'>Nuestro equipo de expertos en madera está listo para superar tus expectativas.</p>
            </div>
          </div>
        </div>


        <p className='mt-14 flex flex-col items-center'>
          <span className='block text-3xl hover:underline hover:cursor-pointer mb-3'>
           ¡Haz realidad tus sueños en madera hoy!
          </span>  
          <span className='w-3/4 block text-2xl'>
            Contáctanos para una consulta gratuita y descubre cómo podemos crear espacios personalizados  que te hagan sentir en casa.
          </span>
        </p>
        <button className='mt-8 text-xl border border-amber-600 hover:bg-amber-400 hover:border-amber-400 p-3 px-6 rounded-lg '>¡Solicita una Consulta Gratuita!</button>
      </div>
    </div>
  )
}
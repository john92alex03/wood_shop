"use client"
import { Formik, Form, Field, ErrorMessage } from "formik"


export default function Contact () {
  return (
    <div className="py-10 h-auto">
      <section className=" w-full justify-between grid grid-cols-2 justify-items-center">
        <div className="flex justify-center p-10">
          
          <Formik 
            initialValues={{ name : '', phone: '', message : '' }}
            validate={ values => {
              const errors = { name : '', phone: '', message : '' }

              if(!values.name){
                errors.name = 'requerido'
              } else if(!values.phone){
                errors.name = 'requerido'
              } else if(!values.message){
                errors.name = 'requerido'
              }

              return errors
            }}  

            onSubmit={ values => {
              console.log(values)
            }}
          >
          {
            ({  }) => (
 
              <Form action="" className="border h-auto w-auto text-center py-24 px-24 rounded-lg shadow-xl z-50">
                <h2 className="text-5xl mb-8 font-serif font-medium select-none">Contactanos!</h2>

                <label htmlFor="name" className="block text-2xl font-serif mb-2 capitalize">name</label>
                <Field type='text' name='name' id='name' className='h-9 w-full pl-1 rounded-md focus:outline-none focus:ring-2 focus:ring-amber-400'  />

                <label htmlFor="phone" className="block text-2xl font-serif mb-2 mt-3 capitalize">telefono</label>
                <Field type='tel' name='phone' id='phone' className='h-9 w-full pl-1 rounded-md focus:outline-none focus:ring-2 focus:ring-amber-400' />

                <label htmlFor="Message" className="block text-2xl font-serif mb-2 mt-3 capitalize">escribre tu mensaje</label>
                <Field type='text' as='textarea' name='message' id='message' className='h-24 p-2 pl-1 w-full rounded-md focus:outline-none focus:ring-2 focus:ring-amber-400' />

                <button type="submit" className="h-14 w-36 shadow border font-serif text-xl border-amber-600 hover:bg-amber-400 hover:border-amber-400  rounded-full mt-8 capitalize ">enviar</button>
              </Form>
            )
          }

          </Formik>

          
        </div>

        <div className="flex p-10 w-11/12 flex-col">
          <p className="text-5xl font-mono mb-6 pt-8 w-9/12">cuentame de lo que necesitas. </p>

          <div className="grid grid-cols-2 w-full">
            <div className="h-36 rounded-md shadow-lg w-9/12 p-5 mt-8">
              <h3 className="text-2xl font-serif capitalize">localizacion</h3>
              <p className="text-lg mt-6 font-extralight">Crr 24 #26-30 b/ san fernando</p>
              <p className="text-lg font-extralight">Jamundi, valle del cauca</p>
            </div>

            <div className="h-36 rounded-md shadow-lg w-9/12 p-5 mt-8">
              <h3 className="text-2xl font-serif capitalize">direccion email</h3>
              <p className="text-lg mt-6 font-extralight">foodshop@gmail.com</p>
            </div>

            <div className="h-36 rounded-md shadow-lg w-9/12 p-5 mt-8">
              <h3 className="text-2xl font-serif capitalize">
                numero de telefono
              </h3>
              <p className="text-lg mt-6 font-extralight">
                +57 <span className="ml-2">321 451 26 70</span>
              </p>
            </div>

            <div className="h-36 rounded-md shadow-lg w-9/12 p-5 mt-8">
              <h3 className="text-2xl font-serif capitalize">
                como puedo ayudarte
              </h3>
              <p className="text-lg mt-6 font-extralight">escribenos para saber que quieres</p>
            </div>
          </div>
        </div>

      </section>
    </div>
  )
}
"use client"

import './globals.css'
import { Inter } from 'next/font/google'
import Navigate from '@/components/Navigate'
import Footer from '@/components/Footer'
import { store } from '../store/store'
import { Provider } from 'react-redux'

const inter = Inter({ subsets: ['latin'] })


export default function RootLayout({ children, }: { children: React.ReactNode }) {
  return (
      <Provider  store={store} >
    <html lang="en" suppressHydrationWarning >
        <body className='min-h-screen min-w-full bg-gradient-to-br from-gray-100 to-gray-300'>
          <Navigate />
          {children}
          <Footer />
        </body>
    </html>
      </Provider>
  )
}

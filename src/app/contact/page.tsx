
import Contact from '@/components/Contact'
import ShowMap from '@/components/ShowMap'

export default function ContactPage ():JSX.Element {
  
  return (
    <div className="min-h-screen text-center flex flex-col">
      <Contact />
      <ShowMap />
    </div>
  )
}


import { NextRequest, NextResponse } from 'next/server'
import { createPool } from '../../../utils/connectDb'


export async function GET (req : NextRequest) {

    const connect = createPool().connect()

    let dataQuery = await connect.then( async (client: any)=>{
            const data = await client.query(`SELECT * FROM categories`)

            return data.rows
        } )
  
  return new NextResponse(JSON.stringify({ response: dataQuery }),
        {
          headers: {
            "Content-Type" : "application/json"
          },
          status: 201
        }
  )
    
}

import { NextRequest, NextResponse } from 'next/server'

import { createPool, closePool } from '../../../../utils/connectDb'

import { v2 } from 'cloudinary'

v2.config({ 
  cloud_name: 'dijl0d3mr', 
  api_key: '828815268291283', 
  api_secret: 'lV6iCCF5bgv60c00KcjFqJTThcM',
  
});


export async function GET (req: NextRequest, {params} : any) {

  const connect = createPool().connect()

  let images_product: any[] = [

  ]

  const { item } = params

  // obtencion de datos BD
  let date = await connect.then( async (client:any)=> {
      let dat = await client.query(`SELECT * from products WHERE ${item} = id_product`).then((response: any)=>  { 
                   client.release()
                   return response.rows[0]
          } )
      
      let images = await client.query(`SELECT name_image, priority_image from imageproducts WHERE ${item} = id_product`)
      
      images.rows.map( async (urlImage: any) => {

        const {name_image , priority_image } = urlImage

        const images = await v2.url(name_image)
        images_product.push({ images , priority_image })
      })
      
      return dat
    })

    let resultEnd = {...date, images_product}
    
  
  return new NextResponse(JSON.stringify(resultEnd),{
    headers : {
      "Content-Type" : "application/json"
    },
    status : 200
  })
  
}


export async function PUT(req: NextRequest, {params} : any) {

   let connect = createPool().connect()

    const { name, description , categorie, idProduct } = await req.json()

    let date = await connect.then( async( client : any )=> {
      let query = await client.query(`UPDATE products SET name_product = $1,
                                        description_product = $2,
                                        modify_product = CURRENT_TIMESTAMP,
                                        category_product = $3
                                      WHERE id_product = $4`, [ name, description, categorie , idProduct ])
                                      .then(( response : any)=> { 
                                                    client.release()
                                                    if(response.rowCount <= 0){
                                                      return 'error al modificar item'
                                                    } else {
                                                      return 'item modificado correctamente'
                                                    }
                                                   
                                                })

      return query
    })  

    return new NextResponse(JSON.stringify({ response : date}), {
      headers : {
        "Content-Type" : "application/json"
      },
      status : 200
    })
}

export async function DELETE (req: NextRequest, { params }:any ) {

  const connect = createPool().connect()

  const { item } = params

  let dataQuery = await connect.then( async(client : any)=> {

                                  let responseQuery = await client.query(`DELETE from products WHERE id_product = ${item}`).then((response : any)=> {
                                            client.release()
                                            if (response.rowCount <= 0) {
                                              return 'error al eliminar el item'
                                            } else {
                                              return 'item elimado correctamente'
                                            }
                                          } )
                                  
                                  return responseQuery
                                })

  return new NextResponse(JSON.stringify({ response : dataQuery}), {
      headers: {
        "Content-Type" : "application/json"
      },
      status : 300
  })
}


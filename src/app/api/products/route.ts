
import { NextRequest, NextResponse } from "next/server";
import { createPool, closePool } from '../../../utils/connectDb'




export function GET (req: NextRequest , {params}:any ) {

    let connect = createPool().connect()

    console.log(params)
    
    return connect.then( (client : any) => {
        return client.query('SELECT * FROM products').then( async (res: any) => {
                            client.release()
                            return await NextResponse.json({ response: res.rows })
                            })
    }).catch( (e: any) => console.log('error conexion base'))
}



export async function POST ( req: NextRequest ) {

    let connect = createPool().connect()

    const { name, description, categorie } = await req.json()

    const dataQuery = await connect.then( async (client : any) => {
                            const query = await client.query(`INSERT INTO products ( 
                                                                                    name_product,
                                                                                    description_product,
                                                                                    creation_product,
                                                                                    modify_product,
                                                                                    category_product
                                                                                ) VALUES (
                                                                                    $1,
                                                                                    $2,
                                                                                    CURRENT_TIMESTAMP,
                                                                                    CURRENT_TIMESTAMP,
                                                                                    $3
                                                                                )`, [ name , description, categorie ]                     
                                                            ).then( (response : any)=> {
                                                                client.release()
                                                                if(response.rowCount <= 0){
                                                                    return 'error al guardar el item'
                                                                } else {
                                                                    return 'item registrado correctamente'
                                                                }
                                                            }).catch( (error : any)=> {
                                                                return 'error al registrar el item'
                                                            })
                            return query
                        })
    
    return new NextResponse(JSON.stringify({ response: dataQuery }),
                        {
                            headers: {
                                "Content-Type" : "application/json"
                            },
                            status : 202
                        }
                    )   
    
}

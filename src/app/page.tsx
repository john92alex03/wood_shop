"use client"

// import Components

import './globals.css'
import { useState } from 'react'

import Navigate from '@/components/Navigate'
import See_items from '@/components/See-items'
import Footer from '@/components/Footer'
import PanelHome from '@/components/PanelHome'
import SeeValoration from '@/components/SeeValoration'
import Contact from '@/components/Contact'

import Image from 'next/image'

interface ItemsDateInterface {
  title: string,
  description: string,
  photo: string,
  idItem : number
}


export default function Home() {

  const [ dataItems, setDataItems] = useState<ItemsDateInterface[] | null>(dataItem)
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [ dataValoration, setDataValoration ] = useState<Array<object>|null>([]) 
    
  return (
    <main suppressHydrationWarning
     className="min-h-screen min-w-full bg-gradient-to-br from-gray-100 to-gray-300">
        
        <header className='relative h-full w-full text-center flex items-center flex-col'> 
          <Image alt='imageBanner' src='/banner_inicio.jpg' width={10000} height={10000} className='imagenBanner'  />
          <h1 className='absolute font-sans font-bold mt-6 text-5xl text-gray-100'>bienvenidos a Wood Shop:<br/><span className='text-3xl '>Tu Destino en Madera</span> </h1>
          <p className='absolute block w-4/12 top-36 text-2xl font-sans text-amber-200 font-medium'>
           Sumérgete en el encanto de la madera con Wood Shop. Nos enorgullece ofrecerte una experiencia excepcional en la creación de mobiliario de madera a medida
          </p>
        </header>

        <PanelHome />
        <See_items dataItems={dataItems}   />
        <SeeValoration />
        <Contact />
    </main>
  )
}

const dataItem = [
  {
    title : 'comedores en madera',
    description : 'Estilo y sostenibilidad en cada detalle',
    photo: 'imageItems/image_comedor.jpg',
    idItem : 1,
    idCategory: 3
  },
  {
    title : 'Sillas en Madera',
    description : 'Inspira tu Decoración con Elementos Auténticos',
    photo: 'imageItems/image_silla_item.jpg',
    idItem : 2,
    idCategory: 1

  },
  {
    title : 'Armarios en Madera',
    description : 'Elegancia y Funcionalidad para tu Espacio',
    photo: 'imageItems/armario-items.jpeg',
    idItem : 3,
    idCategory: 2

  },
  {
    title : 'salas en Madera',
    description : 'Elegancia para tu Espacio',
    photo: 'imageItems/image_sala_item.jpg',
    idItem : 4,
    idCategory: 3

  }

]


import BannerPersonalization from '@/components/personalization/bannerPersonalization'

import { BiSolidChevronsDown } from 'react-icons/bi'

import Image from 'next/image'

export default function services ():JSX.Element {
  
  return (
    <div className='flex flex-col'>
      <div className="h-screen flex flex-col">
        <section className="h-5/6 w-full absolute block">
          <Image alt='imageBannerServices' src='/services/img_banner_servicio.jpg' className='h-full w-full' width={1200} height={1200} />
          <h1 className='text-center relative bottom-56 text-white font-semibold font-serif text-7xl'>wood shop</h1>
          <h3 className='text-center relative bottom-52 text-white font-semibold font-serif text-4xl'>diseños personalizados</h3>
          <div className='h-20 w-full text-center flex justify-center'>
            <BiSolidChevronsDown className='h-16 w-14 pb-3 text-white relative bottom-48 hover:bottom-44 hover:cursor-pointer hover:animate-fade-in-y' />
          </div>
        </section>
      </div>
      <BannerPersonalization />
    </div>
  )

}
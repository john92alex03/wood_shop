
export default function About ():JSX.Element {
  return (
    <div className="min-h-screen text-center">
      <h1 className="text-5xl font-serif font-medium mt-16 capitalize">
        nuestra pasión por la madera
      </h1>
      <h2 className="text-3xl font-serif font-medium mt-8">
        Quiénes somos y cómo creamos magia en madera
      </h2>

      <div className="h-96 flex items-center justify-around mt-20">
        <div className="w-5/12 text-start">
          <h1 className="capitalize text-4xl font-serif">
            ¿Quiénes Somos?
          </h1>
          <p  className=" text-2xl font-serif mt-6">
            En wood Shop, somos apasionados por la madera y su capacidad de transformar espacios en hogares cálidos y acogedores. Nos enorgullece ser artesanos de la madera con una profunda conexión con este noble material. hemos dedicado nuestro tiempo y esfuerzo a diseñar y crear artículos de madera excepcionales que elevan la estética y funcionalidad de cualquier espacio.
          </p>
        </div>
        <h3 className="text-4xl w-72 font-serif">
         Descubre Nuestra Historia y Pasión por la Madera
        </h3>
      </div>

      <div className="h-96 flex items-center justify-around">
        <h3 className="text-4xl w-72 font-serif">
          Nuestra Visión: Liderar la Revolución de la Madera en el Hogar
        </h3>
        <div className="w-5/12 text-start">
          <h1 className="capitalize text-4xl font-serif">
            vision
          </h1>
          <p  className=" text-2xl font-serif mt-6">
            Nuestra visión es convertirnos en líderes en la industria del mobiliario de madera, reconocidos por la excelencia en diseño y calidad. Buscamos ser una fuente confiable de inspiración para quienes buscan muebles únicos y sostenibles que resistan el paso del tiempo. Queremos contribuir a la creación de hogares donde cada pieza de mobiliario cuente una historia de artesanía y elegancia.
          </p>
        </div>
      </div>

      <div className="h-96 flex items-center justify-around">
        <div className="w-5/12 text-start">
          <h1 className="capitalize text-4xl font-serif">
            misión
          </h1>
          <p  className=" text-2xl font-serif mt-6">
            Nuestra misión es ofrecer a nuestros clientes muebles de madera excepcionales que reflejen nuestra pasión por el diseño y la durabilidad. Trabajamos incansablemente para combinar la belleza natural de la madera con la funcionalidad moderna, creando así piezas que enriquecen la vida de las personas. Nos comprometemos a mantener altos estándares de calidad y sostenibilidad en cada paso de nuestro proceso de fabricación.
          </p>
        </div>
        <h3 className="text-4xl w-72 font-serif">
           Nuestra Misión: Crear Muebles de Madera Excepcionales y Sostenibles
        </h3> 
      </div>

      <div className="h-96 flex items-center justify-around mb-20">
        <h3 className="text-4xl w-72 font-serif">
          Nuestro Compromiso: Calidad y Puntualidad en Cada Pieza de Madera
        </h3>
        <div className="w-5/12 text-start">
          <h1 className="capitalize text-4xl font-serif">
            compromiso con los tiempos estipulados
          </h1>
          <p  className=" text-2xl font-serif mt-6">
            En wood shop, nos enorgullece nuestro compromiso con el trabajo bien hecho y la entrega puntual de nuestros productos. Cada pieza de mobiliario que diseñamos y fabricamos pasa por un riguroso proceso de control de calidad para asegurar que cumpla con nuestros estándares más exigentes.
          </p>
        </div>
      </div>
    </div>
  )
}
 
 
 export const metadata = {
  title: "items-shop",
};
 
 export default function ItemsShopLayout( {children} : { children: React.ReactNode } ):React.ReactElement {

 
    return (
      <div className="py-16" >
        { children  }
      </div>
    )
 }
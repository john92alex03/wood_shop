"use client";

{
  /* importando paginas */
}

import ShowFilter from "@/components/items_shop/ShowFilter";
import ShowItems from "@/components/items_shop/showItems";

import { useSelector } from "react-redux";
import { useEffect, useState } from "react";
import axios from "axios";

interface interfaceOptionsCategorie {
  id_category: number;
  name_category: string;
}


export default function ItemsShop(): JSX.Element {

  const [optionsCategorieItems, setOptionsCategorieItems] = useState([]);

  const [currentItems , setCurrentItems] = useState<number>(0)

  const [ priceItems , setPriceItems ] = useState<number[]>([])
  
  const [ items , setItems ] = useState([{
                                          
                                          }])

  const [ seeFilter , setSeeFilter ] = useState<any[]>([{
                                                       id_product: 4,
                                                       name_product: 'comedor en 4 puestos',
                                                       description_product: 
                                                         'Transforma tu espacio de comedor con elegancia atemporal. Este juego de comedor de 4 puestos en madera de cedro ofrece una experiencia gastronómica única',
                                                       stock_product: false,
                                                       price_product: 1400000,
                                                       heigth_product: 60,
                                                       width_product: 70,
                                                       depth_product: 0,
                                                       material_product: 'madera cedro',
                                                       door_product: 0,
                                                       drawer_product: 0,
                                                       creation_product: '2024-01-24T06:05:38.107Z',
                                                       modify_product: '2024-01-24T06:05:38.107Z',
                                                       category_product: 3
                                                      }])

  // solicitud api recuperacion de las opciones de categorias

  async function getOptionsCategorie() {
    
    
    await axios.get('/api/categories').then( async (res)=> {
                                            const data = await res.data.response
                                            setOptionsCategorieItems(data);
                                           
                                        }).catch((error)=> {
                                            console.log(error)
                                        })
  }

   // solicitud api recuperacion de los items

  async function getItems () {

    await axios.get('/api/products').then( async (res)=> {
                                      const items = await res.data.response
                                      setItems(items)
                                     })

  }
  
  useEffect(() => {
    
    getOptionsCategorie()
    getItems()

  }, [])

  useEffect(()=> {

  },[])
  
  return (
    <div className="flex justify-between px-10">
      <ShowFilter optionsCategorieItems={optionsCategorieItems} currentItems={currentItems} priceItems={priceItems} seeFilter={seeFilter} setSeeFilter={setSeeFilter}  />
      <ShowItems setCurrentItems={setCurrentItems} setPriceItems={setPriceItems} setSeeFilter={setSeeFilter} items={items} seeFilter={seeFilter}  />
    </div>
  );
}

'use client'

import axios from 'axios'
import { useEffect, useState } from 'react'
import { useRouter } from 'next/navigation'
import {  v2 } from 'cloudinary'

import Image from 'next/image'
import { MdOutlineArrowBack} from 'react-icons/md'

import { product } from '../../../../interface/shop/product/interface.product'

export default function ItemDescription (): JSX.Element {  

  interface SendPost {
    method: string;
    url : string;

  }


  const [ dataItem , setDataItem ] = useState<product | any>({ images_product: [] })
  const [ img, setImg ] = useState<string>('')
  // const [] = useState()
  
  const router = useRouter()

  async function getData () {
     await axios.get('/api/products/3').then((response: any)=> {
                                                const data = response.data
                                                setDataItem(data)
                                              }
                                            )
  }

  console.log(dataItem)

  function backHistory () {
    router.back()
  }

  useEffect( ()=> {

    getData()
    
  },[])

  return (
    <div className="w-full flex justify-center items-center">
      <div className="container flex">
        
        <button className='absolute left-16 w-auto ' > <MdOutlineArrowBack className='text-3xl' onClick={()=> backHistory()} /> </button>

        <div className='h-full flex w-full justify-center justify-items-center'>

          <article className='w-4/12 h-4/12 relative'>
            <div className='w-11/12 h-64'>
              <img alt='armario' id='armario' src={img} width={1000} height={1000} className='w-10/12 h-64 ' />
            </div> 
            <div className='mt-4 py-1 w-10/12 h-9/12 flex items-center px-2 space-x-3 overflow-hidden overflow-x-auto'>
              {
                dataItem ? dataItem.images_product?.map((image : any) => {
                  if(!img){
                    setImg(image.images)
                  } 
                  return (<img 
                            src={image.images}
                            key={image.images} 
                            alt={image.images} 
                            id={image.images} 
                            width={800} 
                            height={800} 
                            className='w-1/4' 
                            onClick={()=>{
                                setImg(image.images)
                              }
                            } 
                          />)
                }) : ''
              }
            </div>
          </article>


          <section className='w-auto text-start '>
            <h1 className='text-5xl  font-light font-serif capitalize w-2/5 mb-8'>{dataItem.name_product}</h1>
            <h3 className='text-3xl mt-3 font-mono text-gray-700 '>$ {dataItem.price_product}</h3>

            <div className='flex mt-3 capitalize text-md text-gray-600'>
              envio gratis
            </div>

            <div className='mt-6 text-xl'>
              habilitado: <span className='text-lg text-green-600'>{dataItem.stock_product ? 'in stock' : 'not stock' }</span> 
            </div>

            <p className='w-10/12 mt-6 text-gray-900'>
              {dataItem.description_product}
            </p>

            <div className='mt-12 flex capitalize font-semibold text-lg'>
              cantidad:
              <input name='cantidadPedido' id='cantidadPedido' type='number' className='w-10 ml-10 text-lg text-center' />
            </div>

            <div className='mt-6 text-gray-900'>
              El tiempo de entrega de este producto es de <strong>3</strong> a <strong>8</strong> días hábiles. <br/>
              Su garantía es de 1 año por defectos de fabricación
            </div>

            <div className='mt-7 text-gray-900'>
             <strong>Aviso:</strong> Este producto no incluye elementos decorativos ni ambientación.
            </div>

            
              
            <table className='border-collapse border border-slate-300 w-3/6 h-auto p-5 mt-8'>         
                <thead>
                  <tr>
                    <th className='pb-2 text-lg'>especificaciones:</th>
                  </tr>
                </thead>
                <tbody className='p-5'>
                  <tr>
                    <td className='font-medium'>altura:</td>
                    <td>{dataItem.heigth_product} cm</td>
                  </tr>
                  <tr>
                    <td className='font-medium'>ancho:</td>
                    <td>{dataItem.width_product} cm</td>
                  </tr>
                  <tr>
                    <td className='font-medium'>profundidad:</td>
                    <td>{dataItem.depth_product} cm</td>
                  </tr>
                  <tr>
                    <td className='font-medium'>material:</td>
                    <td>{dataItem.material_product}</td>
                  </tr>
                  <tr>
                    <td className='font-medium'>puertas:</td>
                    <td>{dataItem.door_product}</td>
                  </tr>
                  <tr>
                    <td className='font-medium'>cajones:</td>
                    <td>{dataItem.drawer_product}</td>
                  </tr>
                </tbody>
            </table>
            

            <div className='mt-10 flex w-64 justify-between'>
              <button className='bg-red-400 h-12 w-28 text-lg font-mono font-semibold rounded-md' onClick={()=>{ console.log('añadido al carrito') }} >
                + carrito
              </button>

              <button className='bg-green-500 h-12 w-28 text-lg font-mono font-semibold rounded-md'>
                solicitar
              </button>
            </div>

          </section>

          
        </div>
      </div>
    </div>
  )
}
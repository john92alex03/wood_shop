
export interface product {
  category_product : number,
  creation_product : Date,
  depth_product : number,
  description_product : string,
  door_product : number,
  drawer_product : number,
  heigth_product : number,
  id_product : number,
  material_product : string,
  modify_product : Date,
  name_product : string,
  width_product : number
}

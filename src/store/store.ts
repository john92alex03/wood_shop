

import { configureStore } from '@reduxjs/toolkit'
import categorieReducer from './product/productSlice'

export const store = configureStore({

  reducer: {
    categorie : categorieReducer
  }

})



export type RootState = ReturnType<typeof store.getState>


export type AppDispatch = typeof store.dispatch
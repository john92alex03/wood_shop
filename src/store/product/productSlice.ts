

import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'



export interface categorieState {
  categorie : number,
}

const initialState : categorieState = {
  categorie : 0
}


export const categorieSlice = createSlice({

  name : 'categorieItem',
  initialState ,
  reducers : {
    updateCategorie : (state: any, payload : any) => {
      state.categorie = payload.payload
    }
  }

})


export const { updateCategorie } = categorieSlice.actions

export default categorieSlice.reducer
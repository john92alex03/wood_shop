

var Pool = require('pg-pool')

let pool = new Pool({
  database: process.env.DATABASE_NAME,
  user: process.env.DATABASE_USER ,
  password: process.env.DATABASE_PASSWORD ,
  port : process.env.DATABASE_PORT,
  host: process.env.PGHOST,
  connectionString: process.env.NEON_URL,
  max: 20,
  idleTimeoutMillis: 1000,
})

function createPool () {
  return pool
}

function closePool () {
  return pool.end()
}

export  {
  createPool,
  closePool
}





